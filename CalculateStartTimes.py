import sys
import os
# python3.10 -m pip install tk
# python3.10 -m pip install getmac
from pydub import AudioSegment
import pydub.scipy_effects
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import music21 as m21
from scipy.signal import find_peaks


SEGMENT_MS = 50

def plotSong(song, segment_ms):
    volume = [segment.dBFS for segment in song[::segment_ms]]
    x_axis = np.arange(len(volume)) * (segment_ms / 1000)
    plt.plot(x_axis, volume)

# This was adapted from https://stackoverflow.com/questions/67461179/is-there-a-way-to-read-rests-from-a-musicxml-file-using-music21
def xml_to_list(xml):
    """Convert a music xml file to a list of note events

    Notebook: C1/C1S2_MusicXML.ipynb

    Args:
        xml (str or music21.stream.Score): Either a path to a music xml file or a music21.stream.Score

    Returns:
        score (list): A list of note events where each note is specified as
            ``[start, duration, pitch, velocity, label]``
    """

    if isinstance(xml, str):
        xml_data = m21.converter.parse(xml)
    elif isinstance(xml, m21.stream.Score):
        xml_data = xml
    else:
        raise RuntimeError('midi must be a path to a midi file or music21.stream.Score')

    score = []

    for part in xml_data.parts:
        instrument = part.getInstrument().instrumentName

        for note in part.flat.notes:

            if note.isChord:
                start = note.offset
                duration = note.quarterLength

                for chord_note in note.pitches:
                    pitch = chord_note.ps
                    volume = note.volume.realized
                    score.append([start, start + duration, pitch, volume, instrument])

            else:
                start = note.offset
                duration = note.quarterLength
                pitch = note.pitch.ps
                volume = note.volume.realized
                score.append([start, start + duration, pitch, volume, instrument])

    score = sorted(score, key=lambda x: (x[0], x[2]))
    return score

def calculate_correct_start_times(xml_data, beats_per_minute):
    xml_list = xml_to_list(xml_data)
    df = pd.DataFrame(xml_list, columns=['Start', 'End', 'Pitch', 'Velocity', 'Instrument'])
    #(b/m) / (s/m)
    beats_per_second = beats_per_minute / 60
    start_times = df['Start']
    return start_times / beats_per_second


# xml_data = m21.converter.parse('Ode_to_joy.mxl')

def calculate_start_times(song):
    song = song.high_pass_filter(80, order=4)
    maxVolume = max([segment.dBFS for segment in song[::SEGMENT_MS]])
    peaks, _ = find_peaks([segment.dBFS for segment in song[::50]], distance = 6, height=maxVolume *1.6, prominence=0.65)
    
    
    return peaks * SEGMENT_MS

def calculate_success_rate(song, xml_data, beats_per_minute):
    correct_start_times = calculate_correct_start_times(xml_data, beats_per_minute).unique()
    actual_start_times = calculate_start_times(song)

    updatedTimes = correct_start_times + actual_start_times[0] * SEGMENT_MS
    diffs = []
    for peak in (actual_start_times * SEGMENT_MS):
        min_diff = (np.abs(updatedTimes - peak)).min()
        diffs.append(min_diff)

    mean_difference = np.array(diffs).mean()
    ratio = np.count_nonzero((np.array(diffs) < 200))/ len(diffs)

    return (mean_difference, ratio)
