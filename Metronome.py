import time
import math

class Metronome:
    def __init__(self, canvas, radius, beats_per_second):
        self.radius = radius
        self.started = False
        self.canvas = canvas
        self.beats_per_second = beats_per_second
        self.arc = canvas.create_arc(5, 5, radius * 2, radius * 2, fill="white", start=0, extent=180)
        self.line = canvas.create_line(radius, 0, radius, radius)
        self.should_run = False

    def draw(self):
        current_time = time.time()
        portion = float((self.beats_per_second * (current_time - self.start_time)) % (2))

        if portion < 0.5:
            angle = (1 - (portion / 0.5)) * 90
        elif portion < 1.5:
            angle = 180 * (portion - 0.5)
        else:
            angle = 180 - 90 * (portion - 1.5) / 0.5

        
        limit = 0.1
        if (portion < limit) or (portion > 2.0 - limit) or (portion > 1 - limit and portion < 1 + limit):
            # self.canvas.itemconfig(self.arc, fill='green')
            color = 'green'
        else:
            # self.canvas.itemconfig(self.arc, fill='yellow')
            color = 'black'

        self.canvas.delete(self.line)
        self.line = self.canvas.create_line(self.radius, self.radius, self.radius * (1 + math.cos(math.radians(angle))), self.radius * (1 - math.sin(math.radians(angle))), fill =color, width = 3)
        if self.should_run:
            self.canvas.after(20, self.draw)

    def run(self):
        self.should_run = True
        if not self.started:
            self.started = True
            self.start_time = time.time()
        
        self.canvas.after(20, self.draw)
    
    
    def stop(self):
        self.should_run = False
        self.started = False
