import queue
import sys
import tempfile
import threading
import tkinter as tk
from tkinter import ttk
from tkinter import *
import Metronome as m
import numpy as np
import sounddevice as sd
import soundfile as sf
import boto3
import time
import CalculateStartTimes as calculateUtils
from pydub import AudioSegment
import music21 as m21
import os
import shutil
import json
from decimal import Decimal
from boto3.dynamodb.conditions import Key

s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')
dynamodb_client = boto3.resource('dynamodb')
def file_writing_thread(*, q, **soundfile_args):
    with sf.SoundFile(**soundfile_args) as f:
        while True:
            data = q.get()
            if data is None:
                break
            f.write(data)


class RecGui(tk.Tk):

    def setup_main_screen(self, frame, record_screen, table_screen):
        label = tk.Label(frame, text="Smart Metronome", padx=10, pady=20, font=("Arial", 35))
        label.grid(row=1, column=1)

        record = tk.Button(frame, text="Record Session", padx=10, pady=10, command=lambda:record_screen.tkraise())
        record.grid(row=3, column=1)

        view_scores = tk.Button(frame, text="View Previous Sessions", padx=10, pady=10, command=lambda:table_screen.tkraise())
        view_scores.grid(row=4, column=1)

        frame.grid_rowconfigure(0, weight=1)
        frame.grid_rowconfigure(5, weight=1)
        frame.grid_columnconfigure(0, weight=1)
        frame.grid_columnconfigure(2, weight=1)

    def set_beats_per_second(self, beats_per_second, record_button, stop_recording_button, metronome):
        self.on_stop()
        print(beats_per_second)
        self.metronome.beats_per_second = beats_per_second

    def bpm_value_set(self, event, record_button, stop_recording_button, metronome):
        val = int(self.bpm_entry.get())
        self.set_beats_per_second(float(val) / 60.0, record_button, stop_recording_button, metronome)

    def setup_record_screen(self, frame):
        canvas = tk.Canvas(frame, bg="white", height=150, width=300)

        back_button = tk.Button(frame, text="Back", padx=10, pady=10, state="normal", command=lambda: frame.lower())
        back_button.grid(row=0, column=0, sticky='NW')

        self.metronome = m.Metronome(canvas, 149, 1)
        canvas.grid(row=2, column=1, columnspan=2)

        self.start_recording_button = tk.Button(frame, text="Record", padx=10, pady=10, state="normal", command=self.on_rec)
        self.start_recording_button.grid(row=3, column=1)

        self.stop_recording_button = tk.Button(frame, text="Stop Recording", padx=10, pady=10, state="disabled", command=self.on_stop)
        self.stop_recording_button.grid(row=3, column=2)

        bpm = tk.Label(frame, text="BPM", padx=10, pady=10)
        bpm.grid(row=4, column=1)

        bpm_value = tk.StringVar(master=frame)
        self.bpm_entry = tk.Entry(frame, width=10, textvariable=bpm_value)
        self.bpm_entry.bind("<Return>", lambda e: self.bpm_value_set(e, self.start_recording_button, self.stop_recording_button, self.metronome))
        self.bpm_entry.grid(row=4, column=2)

        frame.grid_columnconfigure(0, weight=1)
        frame.grid_columnconfigure(4, weight=1)
        frame.grid_rowconfigure(0, weight=1)
        frame.grid_rowconfigure(5, weight=1)

    def setup_table_screen(self, frame):
        table = ttk.Treeview(frame)
        table['columns'] = ('date_recorded', 'song_item', 'average_ms_difference', 'rate')
        table['show'] = 'headings'
        
        table.column("date_recorded",anchor=CENTER, width=80)
        table.column("song_item",anchor=CENTER,width=80)
        table.column("average_ms_difference",anchor=CENTER,width=120)
        table.column("rate",anchor=CENTER,width=120)

        table.heading("date_recorded",text="Date Recorded",anchor=CENTER)
        table.heading("song_item",text="Song Name",anchor=CENTER)
        table.heading("average_ms_difference",text="Average MS Difference",anchor=CENTER)
        table.heading("rate",text="Ratio Within 200 MS",anchor=CENTER)

        dynamo_table = dynamodb_client.Table('recorded_songs')
        filtering_exp = Key('account').eq('test')
        values = dynamo_table.query(KeyConditionExpression=filtering_exp).get('Items')

        for (i, value) in enumerate(values):
            table.insert(parent='',index='end',iid=i,text='', values=(
                value['date_recorded'],
                    value['song_item'],
                    value['average_ms_difference'],
                    round(value['rate'] * 100)))
        
        table.grid(row=0, column=1)
        frame.grid_columnconfigure(0, weight=1)
        frame.grid_columnconfigure(2, weight=1)

    stream = None

    def __init__(self):
        super().__init__()

        self.state('zoomed')
        self.title('Smart Metronome')

        main_screen = tk.Frame(self)
        record_screen = tk.Frame(self)
        table_screen = tk.Frame(self)

        self.setup_record_screen(record_screen)
        self.setup_table_screen(table_screen)
        self.setup_main_screen(main_screen, record_screen, table_screen)
        

        for frame in (main_screen, record_screen, table_screen):
            # frame.pack(fill=tk.BOTH, expand=True)
            frame.grid(row=0, column=0, sticky='nsew')

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        main_screen.tkraise()

        # We try to open a stream with default settings first, if that doesn't
        # work, the user can manually change the device(s)
        self.create_stream()

        self.recording = self.previously_recording = False
        self.audio_q = queue.Queue()

        self.protocol('WM_DELETE_WINDOW', self.close_window)

    def create_stream(self, device=None):
        if self.stream is not None:
            self.stream.close()
        self.stream = sd.InputStream(
            device=device, channels=1, callback=self.audio_callback)
        self.stream.start()

    def audio_callback(self, indata, frames, time, status):
        if self.recording:
            self.audio_q.put(indata.copy())
            self.previously_recording = True
        else:
            if self.previously_recording:
                self.audio_q.put(None)
                self.previously_recording = False


    def on_rec(self):
        self.start_recording_button["state"] = "disabled"
        self.stop_recording_button["state"] = "normal"
        self.metronome.run()

        self.recording = True

        filename = 'myrecording.wav'
        if os.path.exists(filename):
            os.remove(filename)
        self.thread = threading.Thread(
            target=file_writing_thread,
            kwargs=dict(
                file=filename,
                mode='x',
                samplerate=int(self.stream.samplerate),
                channels=self.stream.channels,
                q=self.audio_q,
            ),
        )
        self.thread.start()

    def on_stop(self, *args):
        self.recording = False
        self.wait_for_thread()

        self.metronome.stop()

        filename = 'myrecording.wav'
        file_copy = 'myrecording-copy.wav'
        if os.path.exists(file_copy):
            os.remove(file_copy)
        
        shutil.copyfile(filename, file_copy)

        current_time = int(time.time()) #output as datetime.fromtimestamp(time).strftime("%A, %B %d, %Y %I:%M:%S")
        s3_client.upload_file(file_copy, 'iot-ido-metronome-recorded-songs', f'{current_time}')
        s3_object = s3_resource.Object('iot-ido-metronome', 'Ode_to_joy.mxl')
        s3_object.download_file('/tmp/Ode_to_joy.mxl')

        song = AudioSegment.from_file(filename)
        xml_data = m21.converter.parse('/tmp/Ode_to_joy.mxl')
        (mean_difference, rate_of_close_success) = calculateUtils.calculate_success_rate(song, xml_data, self.metronome.beats_per_second * 60)
        recorded_songs = dynamodb_client.Table('recorded_songs')

        item = {
            'account': 'test',
            'date_recorded': current_time,
            'song_item': 'Ode_to_joy',
            'recording_item': f'{current_time}',
            'average_ms_difference': mean_difference,
            'rate': rate_of_close_success
        }
        
        recorded_songs.put_item(Item= json.loads(json.dumps(item), parse_float=Decimal))

    def wait_for_thread(self):
        self.after(10, self._wait_for_thread)

    def _wait_for_thread(self):
        if self.thread.is_alive():
            self.wait_for_thread()
            return
        self.thread.join()

    def close_window(self):
        if self.recording:
            self.on_stop()
        self.destroy()


def main():
    app = RecGui()
    app.mainloop()


if __name__ == '__main__':
    main()